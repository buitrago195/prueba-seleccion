import Vue from 'vue'
import App from './App.vue'
import Character from "./components/Character.vue";
import Characters from "./components/Characters.vue";
import VueRouter from 'vue-router';

Vue.use(VueRouter);
Vue.config.productionTip = false

const router = new VueRouter({
  mode: "history",
  base: __dirname,
  routes: [
    {
      path: "/",
      name: "characters",
      component: Characters
    },
    {
      path: "/:characterId",
      name: "character",
      component: Character
    }
  ]
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
