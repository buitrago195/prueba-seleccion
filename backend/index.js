const express = require('express')
const app = express()
const mongoose = require('mongoose');
var cors = require('cors');

app.use(cors())
app.use(express.json())

const conn = mongoose.createConnection('mongodb://localhost:27017/got', {
  useNewUrlParser: true
});
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const CharacterSchema = new Schema({
  id: ObjectId,
  sexo: String,
  slug: String,
  rank: String,
  casa: String,
  libros: String,
  titulos: String
});

const Characters = conn.model('characters', CharacterSchema);


app.listen(3000, () => {
  console.log('El servidor está inicializado en el puerto 3000');
});


app.get('/', (req, res) =>  {
  res.json({ name: 'Saludos desde express' });
});

app.post('/register', async (req, res) => {
    const data = req.body;
    try {
        const character = await Characters.create(data);
        res.status(201).json(character);
    } catch (error) {
        res.status(500).send({ error: error.toString() });
    }
});

app.get('/characters', async (req, res) => {
  try {
    const characters = await Characters.find({});
    res.status(200).json(characters);
  } catch (error) {
    res.status(500).send({ error: error.toString() });
  }
});

app.get('/character/:id', async (req, res) => {
  try {
    const id = req.params.id
    const character = await Characters.findById(id);
    res.status(200).json(character);
  } catch (error) {
    res.status(500).send({ error: error.toString() });
  }
});
